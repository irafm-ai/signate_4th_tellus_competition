# What is inside?
Source codes used in [Signate 4th Tellus competition](https://signate.jp/competitions/284). The repository contains all the sources used for preprocessing, training, and evaluation. The repository does not contain models (are located at another storage) and neither input images nor labels:
  1.  Folder `preprocessing` includes a script for creating masks with a thicker coastline. 
  2.  In folder `training`, you may find four  scripts used to train four models that we are have used in the final ensemble. 
  3.  The models are finally used to create predictions; the script is placed in folder `inference`.

Because we used custom labels that cannot be, according to the competition rules, redistributed, the repository's main purpose is to show our used pipeline. As the pipeline has been coded by three people with a different skill level, it can be useful for rookies in the area as study material and may help them in future competitions.

In the following text, the main idea is explained.

# Preprocessing
In the preprocessing phase, we aim at how the labels can be converted into a suitable format for further training. In the competition, we use three kinds of labels, always converted into an image representation. 

The first type is the pure coastline. We create it on the basis of manually-labelled labels (which are the thord type) and convert into black/white image (where 0 represents no-coastline, 1 represents coastline). We are providing as a script to do this at `preprocessing/ preprocess_create_coastline_from_manually_labeled.ipynb`.

The second one is the thicked coastline. Because the pure coastline (represented by a line) is too narrow and too difficult for a network to train on such data, it is beneficial to use the provided script to create the coastline thicker. The thicker coastline can be generated using the script available in the repository in the file `preprocessing/preprocess_create_masks.ipynb`. These coastlines are generated from json annotations.

The third type is presented by image labels capturing sea/land/no-data. That means we have taken the input coastline images and impainted areas representing sea/land with different colors (white/black). The places with no original image data are filled with gray (128 in 0-255 scale) color. Sometimes, because we painted it by hand, we slightly miss the precise coastline. As this process was done using common graphic editors, we do not provide any script to support this operation.

# Training
**Models**
We have four models. Their names are 'model_30', 'model_22', 'model_b3_bce', 'model_65536'. All of these models use an input resolution of 512x512 and have the same architecture, U-Net. The details are as follows:
- model_30: effnetB4, log input, four classes (sea, land, no-data, + coastline), Adadelta, a combination of focal and Dice loss, softmax.
- model_22: effnetB4, log input, three classes (sea, land, no-data), Adadelta, a combination of focal and Dice loss, softmax.
- model_b3_bce: EffnetB3, log input, one class (degree of to be the coastline), Adam, bce, sigmoid.
- model_65536: EffnetB3, linear input, one class (degree of to be the coastline), Adam, bce, sigmoid.

A training scripts for every model can be found in the repository in `training` folder.

For the training, we used computers with rtx2080 11GB, rtx2060 8GB, and a Colab. All the models are trained from scratch without a public pre-trained model on ImageNet. According to the rules, we also did not use any external data. The training on Colab with high-performance hardware allows the models to have a bigger batch_size, which leads to better results.

**Augmentation**
Probably the most important is the multi-scale cropping. All our models have a resolution of 512x512px. It would be nice to have higher resolution, but our hardware limited us. To overcome this, we took a random position inside an image and created a crop with an arbitrary size (from the interval [1024,1536] or [512,1536] and resized it into 512x512. 

Our other augmentations are: additive/multiplication change of intensity, flipping, a slight change of aspect ratio, rotating, cutout, and multi-image mosaicing (That means we take part from one image, part from other, and so one (we used four images) to compose a new image. This technique is beneficial when you have a small batch size because of HW limitation 

Because the whole data generator has been written by ourselves, we can also control where the crop was taken. One of our models involved curriculum learning: it was trained on crops, including only land or only sea in the first phase. Such a pre-trained model has been then fine-tuned on crops where are always both sea/land were present. Or, we have models trained directly on crops only, including the coastline.

# Inference

The script used for predictions is placed in the `inference` folder.

**Predictions**
We realized TTA in the following way. We have created overlapping crops with a certain resolution (we used several various resolutions) and resized them into the model resolution. Then, predictions are realized in one huge batch. Finally, the predictions were aggregated. We also considered using some other augmentations such as flipping or intensity change, but we did not want to make the prediction process slow, so we omitted them.

**Postprocessing**
As the predictions were not typically "eye-candy," we have done some post-processing. 
* Firstly, we have smoothened the predictions. Model 'model_22' detected coastline as such areas where both sea and land are detected together in some small window. The result has been thinned. Model 'model_30' made the same, but before thinning, we re-labeled coastline as land. 
* Models 'model_b3_bce' and 'model_65536' took maximum from all columns or rows, according to coastline orientation in the image (left-to-right or top-to-bottom). 

In the end, we have four coastlines from which we have computed the final coastline as a weighted average. Finally, we used our own algorithm to fill the gaps in the coastline. Also, we have here our algorithm for coastline tracking (simply say) trying to join the discontinuous parts of the coastline. Due to the complexity, a more detailed description will be introduced in the final presentation.

# Model ZOO

Models (Keras .h5 files) are available at [OneDrive storage](https://365osu-my.sharepoint.com/:f:/g/personal/marek_vajgl_osu_cz/Enyx4jHHHUdGglBq4pGCaKkBDYDZ7wQQvzxp86HhptQ8ww?e=3hi7YE).

# Requirements
To train and evaluate, the following packages were used:
* Keras 2.3.0
* Tensorflow 1.15.0
* Numpy 1.17.0
* Pandas 0.24.2
* tifffile 2020.8.13
* opencv-contrib-python 3.4.2.16
* skicit-learn 0.21.2
* matplotlib 3.1.0
* segmentation_models 1.0.1
* scipy 1.5.2

# License
Project is distributed under [MIT Licence](https://gitlab.com/irafm-ai/signate_4th_tellus_competition/-/blob/master/LICENSE).